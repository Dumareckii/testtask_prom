//
//  FeedTableViewCell.m
//  TestTask
//
//  Created by Valentin Dumareckii on 4/6/15.
//  Copyright (c) 2015 Dumareckii. All rights reserved.
//

#import "FeedTableViewCell.h"
#import "Order.h"
#import "Item.h"

@interface FeedTableViewCell ()
@property (strong, nonatomic) IBOutlet UILabel *orderInfoLabel;
@property (strong, nonatomic) IBOutlet UILabel *itemInfoLabel;
@property (strong, nonatomic) IBOutlet UILabel *timeLabel;
@end

@implementation FeedTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil] objectAtIndex:0];
    if (self) {
    }
    return self;
}

- (void)setOrder:(Order *)order {
    _order = order;
    _orderInfoLabel.text = [NSString stringWithFormat:@"%@ | №%@ - %@",order.state, order.orderId, order.userName];
    Item * item = [order.items firstObject];
    _itemInfoLabel.text = [NSString stringWithFormat:@"%.02f %@ - %@" , item.itemPrice, item.itemCurrency , item.itemName];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    _timeLabel.text = [dateFormatter stringFromDate:order.date];
}

@end
