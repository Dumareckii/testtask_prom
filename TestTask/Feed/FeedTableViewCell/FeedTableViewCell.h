//
//  FeedTableViewCell.h
//  TestTask
//
//  Created by Valentin Dumareckii on 4/6/15.
//  Copyright (c) 2015 Dumareckii. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Order;

@interface FeedTableViewCell : UITableViewCell
@property (nonatomic , strong) Order * order;
@end
