//
//  FeedModel.m
//  TestTask
//
//  Created by Valentin Dumareckii on 4/6/15.
//  Copyright (c) 2015 Dumareckii. All rights reserved.
//

#import "FeedModel.h"
#import "XmlParser.h"
#import "Order.h"
#import "Item.h"

static NSString * const kXmlUrlString = @"https://my.prom.ua/cabinet/export_orders/xml/306906?hash_tag=e1177d00a4ec9b6388c57ce8e85df009";

@interface FeedModel ()
@property (nonatomic , strong) NSArray * ordersArray;
@property (nonatomic , strong) NSArray * searchResultArray;
@end

@implementation FeedModel

- (instancetype)init {
    self = [super init];
    if (self) {
        _ordersArray = [NSArray new];
    }
    return self;
}

- (void) loadOrdersWithComplitionBlock:(void (^) (NSError * error))complitionBlock {
    
    NSArray *docpaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [docpaths objectAtIndex:0];
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"xml.dat"];
    
    NSData * savedData = [NSData dataWithContentsOfFile:dataPath];
    if (savedData) {
        NSError * parseError = [self parseXML:savedData];
        if (complitionBlock) {
            complitionBlock(parseError);
        }
    }
    
    NSURL *url = [NSURL URLWithString:kXmlUrlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        if (!error) {
            
            [data writeToFile:dataPath atomically:YES];
            
            NSData * savedData = [NSData dataWithContentsOfFile:dataPath];
            NSError * parseError = [self parseXML:savedData];
            if (complitionBlock) {
                complitionBlock(parseError);
            }
        }else {
            if (complitionBlock) {
                complitionBlock(error);
            }
        }
    }];
}


- (NSError*) parseXML:(NSData*)xmlData {
    NSError *error = nil;
    NSDictionary *dict = [XMLReader dictionaryForXMLData:xmlData
                                                 options:XMLReaderOptionsProcessNamespaces
                                                   error:&error];
    
    NSMutableArray * tempOrdersArray = [NSMutableArray new];
    
    for (NSDictionary * orderDictionary in [dict valueForKeyPath:@"orders.order"]) {
        Order * order = [Order new];
        order.address = [orderDictionary valueForKeyPath:@"address.text"];
        order.date = [self getDateFromString:[orderDictionary valueForKeyPath:@"date.text"]];
        order.deliveryType = [orderDictionary valueForKeyPath:@"deliveryType.text"];
        order.userEmail = [orderDictionary valueForKeyPath:@"email.text"];
        order.orderId = [orderDictionary valueForKey:@"id"];
        order.index = [[orderDictionary valueForKeyPath:@"index.text"] intValue];
        order.userName = [orderDictionary valueForKeyPath:@"name.text"];
        order.paymentType = [orderDictionary valueForKeyPath:@"paymentType.text"];
        order.userPhone = [orderDictionary valueForKeyPath:@"phone.text"];
        order.priceUAH = [[orderDictionary valueForKeyPath:@"priceUAH.text"] floatValue];
        order.state = [orderDictionary valueForKey:@"state"];
        
        NSMutableDictionary * itemsDictionary = [orderDictionary valueForKeyPath:@"items"];
        [itemsDictionary removeObjectForKey:@"text"];
        NSMutableArray * tempItemsArray = [NSMutableArray new];
        
        if ([itemsDictionary[@"item"] isKindOfClass:[NSArray class]]) {
            for (NSDictionary * itemDictionary in itemsDictionary[@"item"]) {
                 [tempItemsArray addObject:[self getItemFromDictionary:itemDictionary]];
            }
        }else {
            [tempItemsArray addObject:[self getItemFromDictionary:itemsDictionary[@"item"]]];
        }

        order.items = [tempItemsArray copy];
        tempItemsArray = nil;
        [tempOrdersArray addObject:order];
    }
    _ordersArray = [tempOrdersArray copy];
    tempOrdersArray = nil;
    return error;
}

- (Item *)getItemFromDictionary:(NSDictionary*)dictionary {
    Item * item = [Item new];
    item.itemId = [[dictionary valueForKey:@"id"] intValue];
    item.itemCurrency = [dictionary valueForKeyPath:@"currency.text"];
    item.itemName = [dictionary valueForKeyPath:@"name.text"];
    item.itemPrice = [[dictionary valueForKeyPath:@"price.text"] floatValue];
    item.itemQuantity = [[dictionary valueForKeyPath:@"quantity.text"] floatValue];
    item.url = [NSURL URLWithString:[dictionary valueForKeyPath:@"url.text"]];
    item.imageUrl = [NSURL URLWithString:[dictionary valueForKeyPath:@"image.text"]];
    return item;
}

- (NSDate*)getDateFromString:(NSString*)string {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.timeZone = [NSTimeZone systemTimeZone];
    [dateFormatter setDateFormat:@"dd.MM.yy HH:mm"];
    NSDate *date = [dateFormatter dateFromString:string];
    return date;
}

- (NSInteger) getNumberOfRows {
    return _ordersArray.count;
}

- (NSInteger) getNumberOfRowsInSearchResult {
    return _searchResultArray.count;
}

- (Order*) getOrderForIndex:(NSInteger)index {
    return [_ordersArray objectAtIndex:index];
}

- (Order*) getOrderInSearchResultForIndex:(NSInteger)index {
    return [_searchResultArray objectAtIndex:index];
}

- (void)filterContentForSearchText:(NSString*)searchText
{
    NSMutableArray *searchResults = [self.ordersArray mutableCopy];
    NSString *strippedString = [searchText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSArray *searchItems = nil;
    if (strippedString.length > 0) {
        searchItems = [strippedString componentsSeparatedByString:@" "];
    }
    
    NSMutableArray *andMatchPredicates = [NSMutableArray array];
    
    for (NSString *searchString in searchItems) {
        
        // userName field matching
        NSMutableArray *searchItemsPredicate = [NSMutableArray array];
        
        NSExpression *lhs = [NSExpression expressionForKeyPath:@"userName"];
        NSExpression *rhs = [NSExpression expressionForConstantValue:searchString];
        NSPredicate *finalPredicate = [NSComparisonPredicate
                                       predicateWithLeftExpression:lhs
                                       rightExpression:rhs
                                       modifier:NSDirectPredicateModifier
                                       type:NSContainsPredicateOperatorType
                                       options:NSCaseInsensitivePredicateOption];
       [searchItemsPredicate addObject:finalPredicate];
        
        // price field matching
        lhs = [NSExpression expressionForKeyPath:@"userPhone"];
        rhs = [NSExpression expressionForConstantValue:searchString];
        finalPredicate = [NSComparisonPredicate
                          predicateWithLeftExpression:lhs
                          rightExpression:rhs
                          modifier:NSDirectPredicateModifier
                          type:NSContainsPredicateOperatorType
                          options:NSCaseInsensitivePredicateOption];
        [searchItemsPredicate addObject:finalPredicate];
        
        // orderId field matching
        lhs = [NSExpression expressionForKeyPath:@"orderId"];
        rhs = [NSExpression expressionForConstantValue:searchString];
        finalPredicate = [NSComparisonPredicate
                          predicateWithLeftExpression:lhs
                          rightExpression:rhs
                          modifier:NSDirectPredicateModifier
                          type:NSContainsPredicateOperatorType
                          options:NSCaseInsensitivePredicateOption];
        [searchItemsPredicate addObject:finalPredicate];
        
        NSCompoundPredicate *orMatchPredicates = [NSCompoundPredicate orPredicateWithSubpredicates:searchItemsPredicate];
        [andMatchPredicates addObject:orMatchPredicates];
    }
    
    NSCompoundPredicate *finalCompoundPredicate =
    [NSCompoundPredicate andPredicateWithSubpredicates:andMatchPredicates];
    _searchResultArray = [[searchResults filteredArrayUsingPredicate:finalCompoundPredicate] mutableCopy];
}


@end
