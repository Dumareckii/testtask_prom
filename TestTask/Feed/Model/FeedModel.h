//
//  FeedModel.h
//  TestTask
//
//  Created by Valentin Dumareckii on 4/6/15.
//  Copyright (c) 2015 Dumareckii. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Order;
@interface FeedModel : NSObject

- (void) loadOrdersWithComplitionBlock:(void (^) (NSError * error))complitionBlock;
- (NSInteger) getNumberOfRows;
- (NSInteger) getNumberOfRowsInSearchResult;
- (Order*) getOrderForIndex:(NSInteger)index;
- (Order*) getOrderInSearchResultForIndex:(NSInteger)index;
- (void)filterContentForSearchText:(NSString*)searchText;
@end
