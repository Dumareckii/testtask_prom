//
//  ViewController.m
//  TestTask
//
//  Created by Valentin Dumareckii on 4/6/15.
//  Copyright (c) 2015 Dumareckii. All rights reserved.
//

#import "FeedViewController.h"
#import "FeedModel.h"
#import "FeedTableViewCell.h"
#import "DetailViewController.h"
#import "FeedSearchResultController.h"

@interface FeedViewController () <UITableViewDelegate , UITableViewDataSource, UISearchBarDelegate, UISearchControllerDelegate, UISearchResultsUpdating>
@property (nonatomic , strong) FeedModel * model;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) UIRefreshControl *refreshControl;
@property (strong, nonatomic) UISearchController *searchController;
@property (nonatomic, strong) FeedSearchResultController *resultsTableController;
@end

@implementation FeedViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    _model = [FeedModel new];
    self.title = @"Orders";
    [self showActivityIndicator:YES];
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl setBackgroundColor:[UIColor clearColor]];
    [self.refreshControl addTarget:self action:@selector(reloadOrders) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];
    self.tableView.backgroundView = [[UIView alloc] init];
    
    _resultsTableController = [[FeedSearchResultController alloc] init];
    _searchController = [[UISearchController alloc] initWithSearchResultsController:self.resultsTableController];
    self.searchController.searchResultsUpdater = self;
    [self.searchController.searchBar sizeToFit];
    self.tableView.tableHeaderView = self.searchController.searchBar;
    
    self.resultsTableController.tableView.delegate = self;
    self.searchController.delegate = self;
    self.searchController.searchBar.delegate = self;
    self.definesPresentationContext = YES;

    [_model loadOrdersWithComplitionBlock:^(NSError *error) {
        if (!error) {
            [self showActivityIndicator:NO];
            [self.tableView reloadData];
        }else {
            [[[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
        }
    }];
}


- (void) reloadOrders {
    [_model loadOrdersWithComplitionBlock:^(NSError *error) {
        [self.refreshControl endRefreshing];
        if (!error) {
            [self.tableView reloadData];
        }else {
            [[[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
        }
    }];
}

#pragma mark - UITableViewDelegate methods

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_model getNumberOfRows];
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * cellIdentifier = @"FeedCell";
    FeedTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[FeedTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.order = [_model getOrderForIndex:indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    DetailViewController * detailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"detailViewController"];
    detailVC.selectedIndex = indexPath.row;
    detailVC.model = _model;
    [self.navigationController pushViewController:detailVC animated:YES];
}

- (void) showActivityIndicator:(BOOL)show {
    show ? [_activityIndicator startAnimating] : [_activityIndicator stopAnimating];
    [_tableView setHidden:show];
}


#pragma mark - UISearchBarDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}


#pragma mark - UISearchResultsUpdating

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    
    [_model filterContentForSearchText:searchController.searchBar.text];
    FeedSearchResultController *tableController = (FeedSearchResultController *)self.searchController.searchResultsController;
    tableController.model = _model;
    [tableController.tableView reloadData];
}


@end
