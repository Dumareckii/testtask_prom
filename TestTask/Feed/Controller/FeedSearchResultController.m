//
//  FeedSearchResultController.m
//  TestTask
//
//  Created by Valentin Dumareckii on 4/8/15.
//  Copyright (c) 2015 Dumareckii. All rights reserved.
//

#import "FeedSearchResultController.h"
#import "FeedTableViewCell.h"
#import "DetailViewController.h"

@interface FeedSearchResultController () <UITableViewDelegate , UITableViewDataSource>

@end

@implementation FeedSearchResultController

- (void)viewDidLoad {
    [super viewDidLoad];
    _tableView = [[UITableView alloc] initWithFrame:self.view.frame style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.rowHeight = 60;
    [self.view addSubview:_tableView];
}

#pragma mark - UITableViewDelegate methods

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_model getNumberOfRowsInSearchResult];
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * cellIdentifier = @"FeedCell";
    FeedTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[FeedTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.order = [_model getOrderInSearchResultForIndex:indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    DetailViewController * detailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"detailViewController"];
    detailVC.selectedIndex = indexPath.row;
    detailVC.model = _model;
    [self.navigationController pushViewController:detailVC animated:YES];
}

@end
