//
//  FeedSearchResultController.h
//  TestTask
//
//  Created by Valentin Dumareckii on 4/8/15.
//  Copyright (c) 2015 Dumareckii. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FeedModel.h"

@interface FeedSearchResultController : UIViewController
@property (nonatomic , strong) UITableView * tableView;
@property (nonatomic , strong) FeedModel * model;
@end
