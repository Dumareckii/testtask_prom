//
//  DetailCollectionViewCell.h
//  TestTask
//
//  Created by Valentin Dumareckii on 4/7/15.
//  Copyright (c) 2015 Dumareckii. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Order;

@interface DetailCollectionViewCell : UICollectionViewCell

@property (nonatomic , strong) Order * order;

@end
