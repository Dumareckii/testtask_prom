//
//  DetailCollectionViewCell.m
//  TestTask
//
//  Created by Valentin Dumareckii on 4/7/15.
//  Copyright (c) 2015 Dumareckii. All rights reserved.
//

#import "DetailCollectionViewCell.h"
#import "DetailTableViewCell.h"
#import "Order.h"
#import "Item.h"

@interface DetailCollectionViewCell () <UITableViewDelegate , UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UILabel *orderNumberLabel;
@property (strong, nonatomic) IBOutlet UILabel *userNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) IBOutlet UILabel *phoneLabel;
@property (strong, nonatomic) IBOutlet UILabel *emailLabel;
@property (strong, nonatomic) IBOutlet UILabel *adressLabel;
@property (strong, nonatomic) IBOutlet UILabel *companyLabel;
@property (strong, nonatomic) UILabel *footerLabel;
@end

@implementation DetailCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor redColor];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
}


- (void)setOrder:(Order *)order {
    _order = order;
    
    _tableView.tableFooterView = [UIView new];
    UIView * footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 50)];
    _footerLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, footerView.frame.size.width - 10, footerView.frame.size.height - 10)];
    _footerLabel.layer.borderWidth = 2;
    _footerLabel.layer.borderColor = [UIColor blackColor].CGColor;
    _footerLabel.textAlignment = NSTextAlignmentCenter;
    _footerLabel.font = [UIFont fontWithName:@"AvenirNext-DemiBold" size:20];
    [footerView addSubview:_footerLabel];
    
    _tableView.tableFooterView = footerView;
    _orderNumberLabel.text = [NSString stringWithFormat:@"Номер заказа - %@", order.orderId];
    _userNameLabel.text = order.userName;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd.MM.yy HH:mm"];
    NSString *date = [dateFormatter stringFromDate:order.date];

    _dateLabel.text = [NSString stringWithFormat:@"Дата - %@", date];
    _phoneLabel.text = [NSString stringWithFormat:@"Телефон %@", order.userPhone];
    _emailLabel.text = [NSString stringWithFormat:@"Email : %@", order.userEmail];
    _adressLabel.text = [NSString stringWithFormat:@"Адрес доставки : %@", order.address];
    float sum;
    for (Item * item in order.items) {
        sum += item.itemPrice * item.itemQuantity;
    }
    [_footerLabel setText:[NSString stringWithFormat:@"%.2f UAH",sum]];
    [_tableView reloadData];
}

#pragma mark - UITableViewDelegate methods

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.order.items.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * cellIdentifier = @"DetailTableViewCell";
    DetailTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[DetailTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.item = [self.order.items objectAtIndex:indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


@end
