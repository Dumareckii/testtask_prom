//
//  DetailTableViewCell.m
//  TestTask
//
//  Created by Valentin Dumareckii on 4/7/15.
//  Copyright (c) 2015 Dumareckii. All rights reserved.
//

#import "DetailTableViewCell.h"
#import "Item.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface DetailTableViewCell ()
@property (strong, nonatomic) IBOutlet UILabel *profuctNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *quantityLabel;
@property (strong, nonatomic) IBOutlet UILabel *priceLabel;
@end

@implementation DetailTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil] objectAtIndex:0];
    if (self) {
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    _productImageView.layer.cornerRadius = 15;
    _productImageView.layer.masksToBounds = YES;
    _productImageView.layer.borderWidth = 1;
    _productImageView.layer.borderColor = [UIColor colorWithRed:65/255.0 green:110/255.0 blue:152/255.0 alpha:1.0].CGColor;
}


- (void)setItem:(Item *)item {
    _item = item;
    _profuctNameLabel.text = item.itemName;
    _quantityLabel.text = [NSString stringWithFormat:@"%.2f %@ | %.1f шт", item.itemPrice , item.itemCurrency, item.itemQuantity];
    _priceLabel.text = [NSString stringWithFormat:@"%.2f %@", item.itemPrice * item.itemQuantity , item.itemCurrency];
    [self.productImageView sd_setImageWithURL:item.imageUrl
                             placeholderImage:[UIImage imageNamed:@"no-image.jpg"]];

}

@end
