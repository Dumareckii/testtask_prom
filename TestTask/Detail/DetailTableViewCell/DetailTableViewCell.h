//
//  DetailTableViewCell.h
//  TestTask
//
//  Created by Valentin Dumareckii on 4/7/15.
//  Copyright (c) 2015 Dumareckii. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Item;

@interface DetailTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *productImageView;
@property (nonatomic , strong) Item * item;
@end
