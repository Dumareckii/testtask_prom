//
//  DetailViewController.m
//  TestTask
//
//  Created by Valentin Dumareckii on 4/7/15.
//  Copyright (c) 2015 Dumareckii. All rights reserved.
//

#import "DetailViewController.h"
#import "DetailCollectionViewCell.h"

@interface DetailViewController () <UICollectionViewDelegate , UICollectionViewDataSource>
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) IBOutlet UICollectionViewFlowLayout *layout;
@end

static NSString *cellIdentifier = @"detailCollectionViewCell";

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [_collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:_selectedIndex inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
}

#pragma mark - UICollectionView DataSource & Delegate

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [_model getNumberOfRows];
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    DetailCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.order = [_model getOrderForIndex:indexPath.row];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
}

@end
