//
//  DetailViewController.h
//  TestTask
//
//  Created by Valentin Dumareckii on 4/7/15.
//  Copyright (c) 2015 Dumareckii. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FeedModel.h"

@interface DetailViewController : UIViewController
@property (nonatomic , strong) FeedModel * model;
@property (nonatomic) NSInteger selectedIndex;
@end
