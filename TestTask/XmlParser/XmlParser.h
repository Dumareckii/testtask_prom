//
//  XmlParser.h
//  TestTask
//
//  Created by Valentin Dumareckii on 4/6/15.
//  Copyright (c) 2015 Dumareckii. All rights reserved.
//

#import <Foundation/Foundation.h>

enum {
    XMLReaderOptionsProcessNamespaces           = 1 << 0, 
    XMLReaderOptionsReportNamespacePrefixes     = 1 << 1, 
    XMLReaderOptionsResolveExternalEntities     = 1 << 2,
};

typedef NSUInteger XMLReaderOptions;

@interface XMLReader : NSObject <NSXMLParserDelegate>

+ (NSDictionary *)dictionaryForXMLData:(NSData *)data error:(NSError **)errorPointer;
+ (NSDictionary *)dictionaryForXMLString:(NSString *)string error:(NSError **)errorPointer;
+ (NSDictionary *)dictionaryForXMLData:(NSData *)data options:(XMLReaderOptions)options error:(NSError **)errorPointer;
+ (NSDictionary *)dictionaryForXMLString:(NSString *)string options:(XMLReaderOptions)options error:(NSError **)errorPointer;

@end