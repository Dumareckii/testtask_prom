//
//  Order.h
//  TestTask
//
//  Created by Valentin Dumareckii on 4/6/15.
//  Copyright (c) 2015 Dumareckii. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Order : NSObject

@property (nonatomic , strong) NSString * orderId;
@property (nonatomic , strong) NSString * state;
@property (nonatomic , strong) NSString * userName;
@property (nonatomic , strong) NSString * userEmail;
@property (nonatomic , strong) NSString * userPhone;
@property (nonatomic , strong) NSDate * date;
@property (nonatomic , strong) NSString * address;
@property (nonatomic) int index;
@property (nonatomic , strong) NSString * paymentType;
@property (nonatomic , strong) NSString * deliveryType;
@property (nonatomic) float priceUAH;
@property (nonatomic , strong) NSArray * items;

@end
