//
//  Item.h
//  TestTask
//
//  Created by Valentin Dumareckii on 4/6/15.
//  Copyright (c) 2015 Dumareckii. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Item : NSObject
@property (nonatomic) int itemId;
@property (nonatomic , strong) NSURL * imageUrl;
@property (nonatomic , strong) NSString * itemName;
@property (nonatomic) float itemQuantity;
@property (nonatomic , strong) NSString * itemCurrency;
@property (nonatomic , strong) NSURL * url;
@property (nonatomic) float itemPrice;
@end
